# ProPl - Requirement analysis

What are the requirements to be satisfied by the end product?


## Basic Requirements
Core requirements which need more attention
 - [ ] Scheduler - Ability to write To-Do's for each day
 - [ ] Scheduler - Ability to plan out a complete day's work
 - [ ] Scheduler - Setting up events, alarms, remainders for each day
 - [ ] Scheduler - Ability to support recuring tasks
 - [ ] Scheduler - Automated analysis of work done(schedule followed or ToDo's done) (in percentage completed)
 - [x] Diary - Maintain multiple diaries/journals
 - [ ] Notes - A rich text editor to write notes


## Additional Requirements
 - [ ] Diary - Import entries from text file
 - [ ] List and search notes/diaries/scheduler
 - [ ] Cross-Platform integration with data on cloud server(drive/dropbox) for anywhere-anytime access
 - [ ] Offline availability when no internet
 - [ ] Provide sync with other popular calendars(google)
 - [ ] Notes - Highlight and save highlights


## Extra Requirements
 - [ ] Export to md, html, pdf, etc... (for notes, diaries)
 - [ ] Back-up options(google drive backup) for data loss protection
 - [ ] Option to switch to Dark mode or Light mode
 - [ ] vim style key-bindings
 - [ ] Refined editor for notes and diaries (colored editor for markdown and stuff...)
 - [ ] Encryption for data security (with or without password)
 - [ ] Notes - IR search, fuzzy search notes and highlights
 - [ ] Scheduler - set alarm for reminders in kde/windows/system alarm